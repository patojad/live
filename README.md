## Live

Live es una mini WebAPP creada para poder seguir los Streams que realizamos con la comunidad por Twitch desde nuestro sitio, Aparte de integrar el chat y el Stream realizamos trabajo sobre el consumo y el audio para mejorar la calidad en menores anchos de banda. Esto permite a los usuarios una experiencia embebida mucho más placentera.

https://live.patojad.com.ar/
